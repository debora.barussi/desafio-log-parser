# frozen_string_literal: true

require 'json'

# Service to process a log file
class LogParser
  def initialize(file_name)
    @file_name = file_name
    raise Errno::ENOENT unless File.exist?(@file_name)
  end

  def read_first_line
    File.open(@file_name, &:readline)
  end

  def to_json(*_args)
    {
      @file_name => {
        'lines' => count_lines,
        'players' => players,
        'kills' => kills,
        'total_kills' => total_kills
      }
    }.to_json
  end

  private

  def count_lines
    File.foreach(@file_name).count
  end

  def players
    players = []
    File.foreach(@file_name) do |line|
      if line.include? 'ClientUserinfoChanged'
        player_name = line.split('n\\')[1].split('\t')[0]
        players.push(player_name) unless players.include?(player_name)
      end
    end
    players
  end

  def kills
    kills_list = {}
    File.foreach(@file_name) do |line|
      if line.include? 'Kill'
        player_name = line.split('killed')[0].split(': ')[2].strip
        player_killed = line.split('killed')[1].split(' ')[0].strip
        kills_list.store(player_name, 0) unless kills_list.include?(player_name)
        kills_list.store(player_killed, 0) unless kills_list.include?(player_killed)
        kills_list[player_name] += 1
      end
    end
    kills_list
  end

  def total_kills
    total_kills = 0
    File.foreach(@file_name) do |line|
      total_kills += 1 if line.include? 'Kill'
    end
    total_kills
  end
end

# frozen_string_literal: true

require 'simplecov'
SimpleCov.start

require_relative '../../lib/log_parser'
require 'json'

describe LogParser do
  context 'when file does not exists' do
    it 'raises an error' do
      expect { LogParser.new('nonexistentArchive.txt') }.to raise_error(Errno::ENOENT)
    end
  end

  context 'when file exists' do
    describe '#read_first_line' do
      it 'returns a string' do
        expect(LogParser.new('spec/fixtures/games_test.log').read_first_line)
          .to include('This is the first line of this log file.')
      end
    end

    describe '#to_json' do
      it 'returns a json object' do
        expect(LogParser.new('spec/fixtures/games_test.log').to_json).to eq(
          { 'spec/fixtures/games_test.log' => {
            'lines' => 17,
            'players' => ['player1', 'player2', 'new player here'],
            'kills' => { 'Isgalamido': 5, 'Zeh': 1, '<world>': 3, 'player1': 1 },
            'total_kills' => 10
          } }.to_json
        )
      end
    end
  end
end

# desafio-log-parser

# Get started

Install rvm for your operational system. For Ubuntu, check: https://github.com/rvm/ubuntu_rvm

- Install ruby version 3.1.0:
`rvm install ruby 3.1.0`

- Install bundler:
`gem install bundler`

- Clone this repository:
`git clone git@gitlab.com:debora.barussi/desafio-log-parser.git`

- Install the dependencies:
`bundle install`

- Download the `games.log` file. Create the `logs` folder in the diretory root and place the `games.log` file in it.

- To execute the application run:
`ruby main.rb`

- To execute the tests run:
`bundle exec rspec`

# frozen_string_literal: true

require_relative './lib/log_parser'

file_path = 'logs/games.log'
log_parser = LogParser.new(file_path)

puts log_parser.read_first_line

puts log_parser.to_json
